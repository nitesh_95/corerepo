package com.bhushan;

import java.util.Scanner;

public class GenerateDay {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Day of the week");
		int day = scanner.nextInt();

		if (day == 1) {
			System.out.println("Its Monday");
		} else if (day == 2) {
			System.out.println("Its Tuesday");
		} else if (day == 3) {
			System.out.println("Its Wednesday");
		} else if (day == 4) {
			System.out.println("Its Thursday");
		} else if (day == 5) {
			System.out.println("Its Friday");
		} else if (day == 6) {
			System.out.println("Its saturday");
		} else {
			System.out.println("Its Sunday");
		}
	}

}
