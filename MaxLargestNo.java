package com.bhushan;

import java.util.Scanner;

public class MaxLargestNo {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Array List");
		int number = scanner.nextInt();
		int array[] = new int[number];

		for (int i = 0; i < number; i++) {

			System.out.println("Enter no's");
			array[i] = scanner.nextInt();
		}
		int max = array[0];
		for (int i = 0; i < number; i++) {
			if (max < array[i]) {
				max = array[i];
			}
		}
		System.out.println("Maximum value:" + max);
	}
}