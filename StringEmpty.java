package com.bhushan;

import java.util.Scanner;

public class StringEmpty {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the String which u want to find ");
		String str = scanner.nextLine();

		if (str.length() == 0 && str.matches(".*\\d.*")) {
			System.out.println("The String is empty");
		} else {
			System.out.println("The String is not empty");
		}
	}

}
