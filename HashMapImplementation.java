package com.bhushan;

import java.util.HashMap;

public class HashMapImplementation {

	public static void main(String[] args) {

		HashMap<String, String> hashMap = new HashMap<String, String>();
		hashMap.put("IndiaCapital", "New Delhi");
		hashMap.put("Australia", "Canberra");
		System.out.println(hashMap);
		System.out.println(hashMap.get("Australia"));
		System.out.println(hashMap.remove("Australia"));
		System.out.println("The hashMap is"+hashMap);
	}
}
