package com.bhushan;

import java.util.Scanner;

public class VotingEligibility {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws CustomExceptionDefined {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Age of the Person");
		int age = scanner.nextInt();

		try {
			if (age >= 18) {
				System.out.println("The Person is eligible to Vote");
			} else {
				throw new CustomExceptionDefined("The Person is not Eligible to Vote");
			}

		} catch (Exception e) {
			System.out.println("The person is not eligible to vote");
		}

	}

}
