package com.bhushan;

import java.util.Scanner;

public class PrimeList {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		int temp = 0;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the no till which u want to get list");
		int number = scanner.nextInt();

		for (int no = 1; no <= number; no++) {

			for (int i = 2; i < no - 1; i++) {
				if (no % i == 0) {
					temp = temp + 1;
				}
			}
			if (temp == 0) {
				System.out.println(no);
			} else {
				temp = 0;
			}
		}

	}

}
