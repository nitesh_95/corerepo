package com.bhushan;

public class MergingArray {

	public static void main(String[] args) {

		int first_array[] = { 1, 2, 3, 4, 5, 6 };
		int second_array[] = { 7, 8, 9, 10 };

		int first_length = first_array.length;
		int second_length = second_array.length;
		int third_length = first_length + second_length;

		int third_array[] = new int[third_length];

		for (int i = 0; i < first_length; i++) {
			third_array[i] = first_array[i];

		}
		for (int i = 0; i < second_length; i++) {
			third_array[i + first_length] = second_array[i];

		}
		for (int i = 0; i < third_length; i++) {
			System.out.print(third_array[i] + " ");

		}

	}

}
