package com.bhushan;

class Test1 {
	public String test() {
		return "welcome";
	}
}

class Test2 {
	public String test() {
		return "Bhushan_Infotech_Solutions";
	}
}

public class ObjectClass {

	public static void main(String[] args) {

		Test1 test1 = new Test1();
		System.out.println(test1.test());
		System.out.println(test1.getClass());
		Test2 test2 = new Test2();
		System.out.println(test2.test());
		System.out.println(test1.getClass());
	}

}
