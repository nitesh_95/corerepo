package com.bhushan;

import java.util.Scanner;

public class UserInputArrayAddition {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		int array[] = new int[10];

		int sum = 0;

		System.out.print("Enter the Elements");
		for (int i = 0; i <= 5; i++) {
			array[i] = s.nextInt();
		}

		for (int num : array) {
			sum = sum + num;
		}
		System.out.println("Sum is " + sum);
	}

}
