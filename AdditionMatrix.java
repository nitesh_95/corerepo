package com.bhushan;

public class AdditionMatrix {

	public static void main(String[] args) {

		int i, j;
		int firstArray[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		int secondArray[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		int third_Array[][] = new int[3][3];

		for (i = 0; i < 3; i++) {
			for (j = 0; j < 3; j++) {
				third_Array[i][j] = firstArray[i][j] + secondArray[i][j];
				System.out.print(third_Array[i][j] + " ");
			}
			System.out.println();
		}
		
	}

}
