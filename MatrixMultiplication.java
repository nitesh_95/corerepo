package com.bhushan;

public class MatrixMultiplication {

	public static void main(String[] args) {

		int numberArray1[][] = { { 1, 2, 3 }, { 2, 3, 4 }, { 5, 6, 7 } };
		int numberArray2[][] = { { 1, 2, 3 }, { 2, 3, 4 }, { 5, 6, 7 } };

		int numberArray3[][] = new int[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					numberArray3[i][j] = numberArray1[i][k] * numberArray2[j][k];

				}
				System.out.print(numberArray3[i][j] + " ");
			}
			System.out.println();
		}
	}

}
