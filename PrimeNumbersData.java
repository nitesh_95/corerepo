package com.bhushan;

import java.util.Scanner;

public class PrimeNumbersData {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int temp = 0;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number");
		int number = scanner.nextInt();

		for (int i = 2; i <= number - 1; i++) {
			if (number % i == 0) {
				temp = temp + 1;
			}
		}
		if (temp == 0) {
			System.out.println("The no is Prime");
		} else {
			System.out.println("The no is not Prime");
		}
	}

}
