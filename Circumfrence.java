package com.bhushan;

import java.util.Scanner;

class CircumAreaDefined {

	public double circumArea(int radius) {
		double circumfrence = 2 * 3.14 * radius;
		double area = 3.14 * radius * radius;
		System.out.println(circumfrence);
		System.out.println(area);
		return circumfrence;
	}
}

public class Circumfrence {

	public static void main(String[] args) {

		CircumAreaDefined circumAreaDefined = new CircumAreaDefined();
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Radius of the Circle");
		int radius = scanner.nextInt();
		circumAreaDefined.circumArea(radius);
	}
}
