package com.bhushan;

public class ReverseArray {

	public static void main(String[] args) {

		int numberArray1[] = new int[] { 1, 2, 3 };

		for (int elements : numberArray1) {
			System.out.print(elements+ " ");
		}

		for (int i = numberArray1.length - 1; i >= 0; i--) {
			System.out.print(numberArray1[i] + " ");
		}

	}
}