package com.bhushan;

import java.util.Scanner;

public class BaseExponent {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Base");
		int base = s.nextInt();

		System.out.println("Enter the Exponent");
		int exponent = s.nextInt();

		double pow = Math.pow(base, exponent);
		System.out.println(pow);
	}
}