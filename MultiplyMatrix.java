package com.bhushan;

public class MultiplyMatrix {

	public static void main(String[] args) {

		int array1[][] = { { 1, 3, 4 }, { 2, 4, 3 }, { 3, 4, 5 } };
		int array2[][] = { { 1, 3, 4 }, { 2, 4, 3 }, { 3, 4, 5 } };

		int array3[][] = new int[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					array3[i][j] = array1[i][k] * array2[j][k];

				}
				System.out.print(array3[i][j] + " ");

			}
			System.out.println();
		}
	}
}