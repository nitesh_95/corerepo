package com.bhushan;

import java.util.Scanner;

public class Table {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Number");
		int number = s.nextInt();

		for (int i = 1; i <= 10; i++) {
			int table = number * i;

			System.out.println(table);
		}

	}
}