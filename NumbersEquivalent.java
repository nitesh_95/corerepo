package com.bhushan;

import java.util.Scanner;

public class NumbersEquivalent {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the First no");
		int no1 = scanner.nextInt();
		System.out.println("Enter the second no");
		int no2 = scanner.nextInt();
		System.out.println("Enter the third no");
		int no3 = scanner.nextInt();

		if ((no1 == no2) && (no2 == no3) && (no3 == no1)) {
			System.out.println("The nos are  same");
		} else if (no1 == no3) {
			System.out.println("The nos1 and nos3 are same");
		} else if (no2 == no3) {
			System.out.println("The nos2 and nos3 are same");
		} else {
			System.out.println("The nos are not same");
		}

	}

}
