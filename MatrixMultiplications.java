package com.bhushan;

public class MatrixMultiplications {

	public static void main(String[] args) {

		int firstArray[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		int secondArray[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		int thirdArray[][] = new int[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					thirdArray[i][j] = firstArray[i][k] * secondArray[j][k];
				}
				System.out.print(thirdArray[i][j] + " ");
			}
			System.out.println();
		}

	}

}
