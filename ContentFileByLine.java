package com.bhushan;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ContentFileByLine {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException {

		String fileName = "C://Users//admin//Documents//InterviewQuestions.txt";
		File file = new File(fileName);

		Scanner scanner = new Scanner(file);
		while (scanner.hasNextLine()) {
			System.out.println(scanner.nextLine());

		}

	}

}
