package com.bhushan;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class StringDate {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws ParseException {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the String for which date u want date");
		String str = scanner.nextLine();

		Date date = new SimpleDateFormat("dd/mm/yyyy").parse(str);
		System.out.println(date);
	}

}
