package com.bhushan;

import java.util.ArrayList;

public class JoinString {

	public static void main(String[] args) {
		ArrayList<String> arrayList1 = new ArrayList<String>();
		arrayList1.add("welcome to Bhushan");
		ArrayList<String> arrayList2 = new ArrayList<String>();
		arrayList2.add("Infotech Solutions");

		ArrayList<String> joined = new ArrayList<String>();
		joined.addAll(arrayList1);
		joined.addAll(arrayList2);

		System.out.println(joined);
	}

}
