package com.bhushan;

public class AddMatrix {
	public static void main(String[] args) {

		int numberArray1[][] = { { 1, 2, 3 }, { 2, 3, 4 }, { 5, 6, 7 } };
		int numberArray2[][] = { { 1, 2, 3 }, { 2, 3, 4 }, { 5, 6, 7 } };

		int numberArray3[][] = new int[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				numberArray3[i][j] = numberArray1[i][j] + numberArray2[i][j];
				System.out.print(numberArray3[i][j] + " ");
			}
			System.out.println();

		}
	}

}
