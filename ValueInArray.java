package com.bhushan;

import java.util.Scanner;

public class ValueInArray {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		boolean found = false;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the value which u want to find in array");
		int toFind = scanner.nextInt();

		int[] number_array = { 1, 2, 3, 4, 5 };

		for (int elements : number_array) {
			if (toFind == elements) {
				found = true;
				break;
			}
		}
		if (found) {
			System.out.println("The integer is found");
		} else {
			System.out.println("The integer is not found");
		}
	}

}
