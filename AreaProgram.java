package com.bhushan;

class AreaPrograms {

	int triangle = 1;

	public void area(int base, int height) {
		triangle =  (base * height)/2;
		System.out.println(triangle);
	}
}

public class AreaProgram {
	public static void main(String[] args) {

		AreaPrograms areaPrograms = new AreaPrograms();
		areaPrograms.area(20, 30);
	}

}
