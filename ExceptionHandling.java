package com.bhushan;

public class ExceptionHandling {

	public static void main(String[] args) {

		@SuppressWarnings("unused")
		int sum, a = 500, b = 0;

		try {
			sum = a / b;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("The Exception has been Handled");
		} catch (Exception e) {
			System.out.println("The Exception has been handled and cleared");
		}
	}

}
