package com.bhushan;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		int rev = 0, remainder, temp;

		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Number");
		temp = s.nextInt();

		int number = temp;
		while (number != 0) {
			remainder = number % 10;   
			rev = rev * 10 + remainder; 
			number = number / 10;   
		}
		if (temp == rev) {
			System.out.println("The No is Palindrome");
		} else {
			System.out.println("The no is not Palindrome");
		}

	}
}