package com.bhushan;

public class Custom  extends Exception {

	private static final long serialVersionUID = 1L;

	public Custom() {
		super();
		System.out.println("The Person is underage");
	}

	public Custom(String message) {
		super(message);
	}

}
