package com.bhushan;

import java.util.Scanner;

public class CharacterString {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Character");
		char ch = scanner.next().charAt(0);

		String str = Character.toString(ch);
		System.out.println(str);
	}

}
