package com.bhushan;

import java.io.*;
import java.util.Scanner;

public class FileWriter {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		String pathname = "E://welcome";
		String pathNames = "E://welcome//welcome.txt";

		File file = new File(pathname);
		boolean mkdir = file.mkdir();
		try {

			if (mkdir == true) {
				File file2 = new File(pathNames);
				boolean createNewFile = file2.createNewFile();
				System.out.println("The file and folder both has been Created");
				if (createNewFile == true) {
					java.io.FileWriter fileWriter = new java.io.FileWriter(pathNames);
					fileWriter.write("welcome to Bhushan Informatics Center");
					fileWriter.close();
					System.out.println("The file folder and filewriter has been Created");
				} else {
					System.out.println("Something went Wrong");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			File file2 = new File(pathNames);
			Scanner s = new Scanner(file2);
			String data = s.nextLine();
			System.out.println(data);

			System.out.println(file.toString());
		}
	}
}