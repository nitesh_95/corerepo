package com.bhushan;

import java.util.Scanner;

public class NumberFactor {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the no to which u need to find the factors");
		int number = scanner.nextInt();

		for (int i = 1; i <= number; i++) {
			if (number % i == 0) {
				System.out.println("The factors are " + i);
			}

		}
	}
}
