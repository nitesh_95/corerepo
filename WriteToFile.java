package com.bhushan;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class WriteToFile {

	public static void main(String[] args) throws IOException {

		String fileName = "e://welcome.txt";
		File file = new File(fileName);
		if (file.createNewFile() == true) {
			System.out.println("File has been Created");
		} else {
			System.out.println("Unable to Create the file");
		}

		try {
			@SuppressWarnings("resource")
			FileWriter fileWriter = new FileWriter(fileName);
			fileWriter.write("Welcome to bhushan Informatics");
			System.out.println("The File has been written");
			fileWriter.close();

		} catch (FileNotFoundException e) {

			System.out.println("The file has not been found");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("The Block has been Executed");
		}

	}

}
