package com.bhushan;

public class MatrixSubtraction {

	public static void main(String[] args) {

		int array1[][] = { { 1, 2, 3 }, { 2, 3, 4 }, { 5, 6, 7 } };
		int array2[][] = { { 1, 2, 3 }, { 2, 3, 4 }, { 5, 6, 7 } };

		int array3[][] = new int[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				array3[i][j] = array1[i][j] + array2[i][j];
				System.out.print(array3[i][j] + " ");
			}
			System.out.println();
		}
	}
}