package com.bhushan;

public class AverageArrayAddition {

	public static void main(String[] args) {
		int elements[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		int calculation = 0;
		int result = 0;
		for (int data : elements) {
			calculation = (data + calculation);

			result = calculation / elements.length;
		}
		System.out.print(result);

	}

}
