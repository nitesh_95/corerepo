package com.bhushan;

public class TransposeMatrix {

	public static void main(String[] args) {
		int array[][] = { { 1, 3, 4 }, { 2, 4, 3 }, { 3, 4, 5 } };

		int arrayFinal[][] = new int[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				arrayFinal[i][j] = array[j][i];

				System.out.print(arrayFinal[i][j] + "  ");
			}
			System.out.println();
		}
	}
}