package com.bhushan;

import java.util.ArrayList;

public class ArrayListElement {

	public static void main(String[] args) {

		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		arrayList.add(0, 1);
		arrayList.add(1, 2);
		arrayList.add(2, 3);
		arrayList.add(3, 4);
		arrayList.add(4, 5);
		for (Integer str : arrayList) {
			System.out.println(str);

		}
		arrayList.forEach((str -> {
			System.out.println(str);

		}));
	}

}
