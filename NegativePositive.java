package com.bhushan;

import java.util.Scanner;

public class NegativePositive {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number");
		int number = scanner.nextInt();

		if (number > 0) {
			System.out.println("The no is +ve");
		} else {
			System.out.println("The no is -ve");
		}
	}

}
