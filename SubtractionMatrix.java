package com.bhushan;

public class SubtractionMatrix {

	public static void main(String[] args) {
		int a[][] = { { 1, 2, 3 }, { 2, 3, 4 }, { 5, 4, 6 } };
		int b[][] = { { 1, 2, 3 }, { 5, 4, 6 }, { 7, 8, 9 } };

		int c[][] = new int[3][3];

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < b.length; j++) {
				c[i][j] = b[i][j] - a[i][j];
				System.out.print(c[i][j] + " ");
			}
			System.out.println();

		}

	}

}
