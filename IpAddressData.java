package com.bhushan;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class IpAddressData {

	public static void main(String[] args) throws UnknownHostException {

		InetAddress inetAddress = InetAddress.getLocalHost();

		System.out.println(inetAddress.getHostAddress());
	}

}
