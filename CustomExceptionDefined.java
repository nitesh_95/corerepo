package com.bhushan;

public class CustomExceptionDefined extends Exception {

	private static final long serialVersionUID = 1L;

	public CustomExceptionDefined() {
		super();
	}

	public CustomExceptionDefined(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CustomExceptionDefined(String message, Throwable cause) {
		super(message, cause);
	}

	public CustomExceptionDefined(String message) {
		super(message);
	}

	public CustomExceptionDefined(Throwable cause) {
		super(cause);
	}

}
