package com.bhushan;

import java.util.Scanner;

public class FibbonacciSeries {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int a = 0;
		int b = 1, c;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the no upto which u want to get the list");

		int number = scanner.nextInt();
		for (int i = 0; i <= number; i++) {
			c = a + b;
			System.out.println(c);
			a = b;
			b = c;
		}

	}

}
