package com.bhushan;

public class TreeSet {

	public static void main(String[] args) {

		java.util.TreeSet<Integer> treeSet = new java.util.TreeSet<Integer>();
		treeSet.add(26);
		treeSet.add(34);
		treeSet.add(26);
		treeSet.add(34);
		System.out.println(treeSet);
		System.out.println("The lowest is " + treeSet.pollFirst());
		System.out.println("The highest is " + treeSet.pollLast());
	}

}
