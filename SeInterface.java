package com.bhushan;

public class SeInterface implements GetInterface {
	int sum = 0;

	public static void main(String[] args) {

		SeInterface seInterface = new SeInterface();
		seInterface.show();
		seInterface.show2(23, 24);
	}

	@Override
	public void show() {
		System.out.println("The Data is Returned");

	}

	@Override
	public int show2(int x, int y) {
		sum = x + y;
		System.out.println("The Returned sum is" + " "  + sum);
		return sum;
	}

}
