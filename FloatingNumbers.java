package com.bhushan;

import java.util.Scanner;

public class FloatingNumbers {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter First Floating Number");
		float number1 = scanner.nextFloat();
		System.out.println("Enter Second Floating Number");
		float number2 = scanner.nextFloat();

		double value1 = (Math.round(number2));
		double value2 = (Math.round(number1));

		if (value1 == value2) {
			System.out.println("The no is Matching");
		} else {
			System.out.println("The no is not Matching");
		}
	}

}
