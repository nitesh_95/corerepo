package com.bhushan;

import java.util.Scanner;

public class UserInputArrayMultiplication {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int sum = 1;
		int array[] = new int[3];

		Scanner s = new Scanner(System.in);

		System.out.println("Enter the Numbers");
		for (int i = 0; i < 3; i++) {
			array[i] = s.nextInt();
		}
		for (int elements : array) {
			sum = sum * elements;
		}
		System.out.println("The sum is " + sum);
	}

}
