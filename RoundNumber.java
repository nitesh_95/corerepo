package com.bhushan;

import java.util.Scanner;

public class RoundNumber {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number");
		float number = scanner.nextFloat();

		System.out.println(Math.round(number));

	}

}
