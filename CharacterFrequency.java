package com.bhushan;

import java.util.Scanner;

public class CharacterFrequency {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int frequency = 0;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the String");
		String str = scanner.nextLine();

		System.out.println("Enter the Character for which u want to find frequency");
		Character ch = scanner.next().charAt(0);

		for (int i = 0; i < str.length(); i++) {
			if (ch == str.charAt(i)) {
				++frequency;
			}

		}
		System.out.println("No of Time when character is found is " + frequency);
		System.out.println();
	}

}
