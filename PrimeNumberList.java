package com.bhushan;

import java.util.Scanner;

public class PrimeNumberList {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int temp = 0;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter upto which u want the List");
		int no = scanner.nextInt();
		for (int number = 1; number < no; number++) {
			for (int i = 2; i <= number - 1; i++) {
				if (number % i == 0) {
					temp = temp + 1;
				}
			}
			if (temp == 0) {
				System.out.println(number);
			} else {
				temp = 0;
			}
		}
	}

}
