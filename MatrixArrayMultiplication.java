package com.bhushan;

public class MatrixArrayMultiplication {

	public static void main(String[] args) {

		int number1[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
		int number2[] = { 1, 2, 3, 4, 5, 6, 7, 8 };

		int number3[] = new int[number1.length];

		for (int i = 0; i < number3.length; i++) {
			number3[i] = number1[i] * number2[i];

			System.out.print(number3[i] + " ");
		}
		System.out.println();
	}

}
