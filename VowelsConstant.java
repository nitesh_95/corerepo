package com.bhushan;

import java.util.Scanner;

public class VowelsConstant {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Character");
		char ch = scanner.next().charAt(0);

		if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
			System.out.println("The Character is vowel ");
		} else {
			System.out.println("The character is consonant");
		}
	}

}
