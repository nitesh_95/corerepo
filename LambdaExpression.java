package com.bhushan;

import java.util.ArrayList;

public class LambdaExpression {

	public static void main(String[] args) {

		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		arrayList.add(0, 1);
		arrayList.add(1, 2);
		arrayList.add(2, 3);
		arrayList.add(3, 4);
		arrayList.add(4, 5);
		arrayList.add(5, 6);

		arrayList.forEach((n) -> {
			System.out.println("The Numbers" + n);
		});
	}
}
