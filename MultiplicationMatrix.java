package com.bhushan;

public class MultiplicationMatrix {

	public static void main(String[] args) {

		int first_array[][] = { { 1, 2, 3 }, { 2, 3, 4 }, { 5, 6, 7 } };
		int second_array[][] = { { 1, 2, 3 }, { 2, 3, 4 }, { 5, 6, 7 } };

		int third_array[][] = new int[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {

					third_array[i][j] = first_array[i][k] * second_array[j][k];

				}
				System.out.print(third_array[i][j] + " ");

			}
			System.out.println();
		}

	}

}
