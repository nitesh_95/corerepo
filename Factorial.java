package com.bhushan;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {

		int factorial = 1;
		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Number");
		int number = s.nextInt();

		for (int i = 1; i <= number; i++) {
			factorial = factorial * i;
		}
		System.out.println(factorial);
	}
}