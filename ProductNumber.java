package com.bhushan;

import java.util.Scanner;

class Multiplication {

	public int multiply(int number1, int number2) {
		int calculation = number1 * number2;
		System.out.println("The Rquired total calculation is " + calculation);
		return calculation;
	}
}

public class ProductNumber {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the First number");
		int number1 = scanner.nextInt();
		System.out.println("Enter the Second number");
		int number2 = scanner.nextInt();
		Multiplication multiplication = new Multiplication();
		multiplication.multiply(number1, number2);

	}

}
