package com.bhushan;

import java.util.Scanner;

public class Frequency {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		int frequency = 0;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the String Line");
		String str = scanner.nextLine();
		System.out.println("ENTER THE CHARACTER");
		char ch = scanner.next().charAt(0);

		for (int i = 0; i < str.length(); i++) {
			if (ch == str.charAt(i)) {
				++frequency;
			}
		}
		System.out.println(frequency);
	}

}
