package com.bhushan;

class Customs extends Exception {

	private static final long serialVersionUID = 1L;

	public Customs() {
		super();
		System.out.println("The Person is underage");
	}

	public Customs(String message) {
		super(message);
	}

}

class CustomExceptions {

	public int underAgeException(int age) throws Customs {
		if (age < 18) {
			System.out.println("The person is able to vote");
		} else {
			throw new Customs("The person is not able to vote");
		}

		return age;
		
		
		
		
		
		
		
		
		
		
		
		
	}

}

public class CustomException {
	public static void main(String[] args) throws Customs {
		CustomExceptions customExceptions = new CustomExceptions();
		customExceptions.underAgeException(21);
	}
}