package com.bhushan;

class Method {

	public int addition(int x, int y) {
		int c = x + y;
		System.out.println(c);
		return c;

	}
}

public class AddMethod {

	public static void main(String[] args) {

		Method method = new Method();
		method.addition(23, 24);
	}

}
