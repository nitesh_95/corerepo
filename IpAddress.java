package com.bhushan;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class IpAddress {

	public static void main(String[] args) throws UnknownHostException {
		InetAddress inetAddress = InetAddress.getLocalHost();
		System.out.println("The ip of the Systen is " + inetAddress.getHostAddress());
	}
}
